
// Released to public domain

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

	@IBOutlet weak var window: NSWindow!


	func applicationDidFinishLaunching(aNotification: NSNotification) {
		let cp = NSColorPanel.sharedColorPanel()
		cp.setTarget(self)
		cp.setAction(Selector("colorDidChange:"))
		cp.makeKeyAndOrderFront(self)
		cp.continuous = true
	}

	func applicationWillTerminate(aNotification: NSNotification) {
	
	}

	func colorDidChange(sender:AnyObject) {
		if let cp = sender as? NSColorPanel {
			print(cp.color)
			self.window.backgroundColor = cp.color
		}
	}

}

